package network;

import java.util.Arrays;

import neuron.Neuron;
import neuron.StartNeuron;

public class SimpleNetwork implements INetwork{

	private static double CHANGE_COEFFICIENT = 3;

	
	protected int numin;
	protected int numout;
	
	protected StartNeuron[] inputNeurons;
	protected Neuron[] outputNeurons;
	
	protected double[] weights;
	
	public SimpleNetwork(int nums) {
		this.numin = nums;
		this.numout = nums;
		
		
		
		weights = new double[nums*nums];
		Arrays.setAll(weights, (i)->Math.random() );
		
		setUpNeurons();
	}
	
	public SimpleNetwork(int nums,double[] weights) {
		this(nums);
		this.weights = weights;
		setUpNeurons();
	}
	
	
	private void setUpNeurons() {
		this.inputNeurons = new StartNeuron[this.numin];
		this.outputNeurons = new Neuron[this.numout];
		
		
		Arrays.setAll(inputNeurons, (i)->(new StartNeuron()));
		
		int numweights = 0;
		for(int i=0;i<outputNeurons.length;i++) {
			
			double[] newone = Arrays.copyOfRange(weights, numweights, this.numin+numweights);
			
			Neuron n = new Neuron(inputNeurons,newone);
			
			outputNeurons[i] = n;
			
			numweights += this.numin;
		}
	}
	
	@Override
	public double[] getOutput(double[] input) {
		
		//TODO put illegal state exceptions
		
		for(int i=0;i<inputNeurons.length;i++) {
			StartNeuron sn = inputNeurons[i];
			sn.setValue(input[i]);
		}
		
		double[] out = new double[outputNeurons.length];
		for(int i=0;i<outputNeurons.length;i++) {
			out[i] = outputNeurons[i].getValue();
		}
		return out;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public SimpleNetwork getDescendent() {
		double[] nweights = Arrays.copyOf(weights, weights.length);
		int ndex = (int)(Math.random()*nweights.length);
		double weight = nweights[ndex];
		nweights[ndex] = weight + (Math.random()-0.5)*weight/CHANGE_COEFFICIENT;
		
		return new SimpleNetwork(this.numout,nweights);
	}

	
	@Override
	public String toString() {
		return super.toString() + ": " + Arrays.toString(this.weights);
	}

}
