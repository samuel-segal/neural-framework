package network;

public interface INetwork{
	
		
	double[] getOutput(double[] input);
	
	<T extends INetwork> T getDescendent();
	
	
}