package main;

import handler.NetworkHandler;
import network.SimpleNetwork;

public class Main {
	public static void main(String[] args) throws Exception{
		
		NetworkHandler handler = new NetworkHandler( ()->new SimpleNetwork(2),20);
		for(int i=0;i<0xFFF;i++) {
			handler.step();
		}
	}
}
