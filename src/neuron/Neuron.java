package neuron;

import java.util.Arrays;

public class Neuron implements INerveEnd{

	
	private Dendrite[] dendrites;
	
	private double cachedOut;
	private boolean hasGenValue = false;
	
	
	public Neuron(INerveEnd[] connectedNeurons,double[] weights) {
		dendrites = new Dendrite[connectedNeurons.length];
		
		for(int i=0;i<connectedNeurons.length;i++) {
			
			Dendrite den = new Dendrite(connectedNeurons[i],weights[i]);
			dendrites[i] = den;
		}
	}
	
	@Override
	public double getValue() {
		
		//Calculates arithmetic weight
		if(!hasGenValue) {
			for(Dendrite den:dendrites) {
				cachedOut += den.getWeight()*den.getNeuron().getValue();
			}
			hasGenValue = true;
		}
		return cachedOut;
	}
	
	public Dendrite[] getDendrites() {
		return dendrites;
	}

	
	@Override
	public String toString() {
		return super.toString()+": "+Arrays.toString(dendrites);
	}
	
}
