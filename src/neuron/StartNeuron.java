package neuron;

public class StartNeuron implements INerveEnd{

	private double value;
	
	
	public StartNeuron() {
		this(0);
	}
	
	public StartNeuron(double val) {
		this.value = val;
	}

	@Override
	public double getValue() {
		return value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
}
