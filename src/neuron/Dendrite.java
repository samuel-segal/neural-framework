package neuron;

//Not using a record here for portability
public class Dendrite{
	private INerveEnd neuron;
	private double weight;

	public Dendrite(INerveEnd n, double d) {
		this.neuron = n;
		this.weight = d;
	}
	
	public INerveEnd getNeuron() {
		return neuron;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public void setNeuron(INerveEnd neuron) {
		this.neuron = neuron;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	@Override
	public String toString() {
		return weight+"";
	}

}

