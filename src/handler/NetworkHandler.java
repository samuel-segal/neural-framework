package handler;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import network.INetwork;
import teacher.AdditionTeacher;
import teacher.ITeacher;

public class NetworkHandler{
	
	private int numNetworks;
	
	public List<INetwork> networks;
	public ITeacher teacher;
	
	public NetworkHandler(Supplier<INetwork> defaultState, int numNetworks) {
		this.numNetworks = numNetworks;
		teacher = new AdditionTeacher();
		
		networks = new ArrayList<INetwork>();
		for(int i=0;i<this.numNetworks;i++) {
			networks.add( defaultState.get() );
		}
	}
	
	public void step() {
		
		double minValue = Double.MAX_VALUE;
		INetwork min = null;
		
		teacher.changeTestingValues();
		
		for(int i=0;i<networks.size();i++) {
			INetwork net = networks.get(i);
			double value = teacher.gradeError(net);
			System.out.println("Err: "+value);
			if(value<minValue) {
				minValue = value;
				min = net;
			}
			
		}
		
		networks.clear();
		System.out.println(min);
		for(int i=0;i<this.numNetworks;i++) {
			INetwork descendent = min.getDescendent();
			networks.add(descendent);
		}
		
		
	}
	
	public INetwork getPrimeNetwork() {
		return this.networks.get(0);
		
	}
}