package teacher;

import network.INetwork;

public interface ITeacher {
	
	void changeTestingValues();
	
	//The lower grade the better
	double gradeError(INetwork network);
}
