package teacher;

import network.INetwork;

public class AdditionTeacher implements ITeacher{

	
	double[] values;
	

	@Override
	public void changeTestingValues() {
		 values = new double[] {Math.random()*20,Math.random()*20};
		 System.out.println("Values: "+values[0]+" , "+values[1]);
	}

	@Override
	public double gradeError(INetwork network) {
		double[] result = network.getOutput(values);
		double ztarget = values[0]+values[1];
		
		return Math.abs(ztarget-result[0]-result[1]);
	}
	
	

}
